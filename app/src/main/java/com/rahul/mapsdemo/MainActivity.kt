package com.rahul.mapsdemo

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.statusBarColor = resources.getColor(R.color.white)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        bottomNavigationView()
        defaultFrag()
    }

    private fun defaultFrag() {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_layout, MapsFragment())
        transaction.commit()
    }

    private fun bottomNavigationView() {
        nav_view.setOnNavigationItemSelectedListener { menuItem: MenuItem ->
            var selectedFragment: Fragment? = null
            when (menuItem.itemId) {
                R.id.navigation_home -> selectedFragment = MapsFragment()
                R.id.navigation_orders -> selectedFragment = LiveLocation()
            }
            val transaction =
                supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frame_layout, selectedFragment!!)
            transaction.commit()
            true
        }
    }

}