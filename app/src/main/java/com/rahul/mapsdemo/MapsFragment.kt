package com.rahul.mapsdemo

import android.content.IntentSender.SendIntentException
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import org.json.JSONObject

class MapsFragment : Fragment() {

    private var mMap: GoogleMap? = null
    private val REQUEST_LOCATION_PERMISSION = 1
    protected val REQUEST_CHECK_SETTINGS = 0x1
    private var gpsTracker: GpsTracker? = null
    private var latitude = 0.0
    private  var longitude: Double = 0.0
    private var home: LatLng? = null
    private var destinationHome:LatLng?=null
    private var client: GoogleApiClient? = null
    lateinit var destLocation: String

    private val callback = OnMapReadyCallback { googleMap: GoogleMap ->
        getCurrentLocation(
            googleMap
        )
    }

    private fun getCurrentLocation(googleMap: GoogleMap) {
        //requestGPSSettings();
        mMap = googleMap
        gpsTracker = GpsTracker(requireContext())
        if (gpsTracker!!.canGetLocation()) {
            latitude = gpsTracker!!.getLatitude()
            longitude = gpsTracker!!.getLongitude()
        } else {
            client = getAPIClientInstance()
            if (client != null) {
                client!!.connect()
            }
            requestGPSSettings()
        }
        mMap!!.uiSettings.isZoomControlsEnabled = true
        home = LatLng(latitude, longitude)
        Log.i("Coordinates Location",latitude.toString()+"\n"+longitude.toString())
        Toast.makeText(requireContext(),latitude.toString()+"\n"+longitude.toString(),Toast.LENGTH_LONG).show()
        destinationHome= LatLng(26.1426, 91.6435)

        mMap!!.addMarker(MarkerOptions().position(home!!).title("Origin"))
        mMap!!.addMarker(MarkerOptions().position(destinationHome!!).title("Destination"))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLng(home))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(home!!, 14.5f))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(destinationHome!!, 12f))

        val path: MutableList<List<LatLng>> = ArrayList()
        val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?origin=$latitude,$longitude&destination=26.1426,91.6435&key=${resources.getString(R.string.api_key)}"

        val directionsRequest = object : StringRequest(Method.GET, urlDirections, Response.Listener { response ->
            val jsonResponse = JSONObject(response)
            // Get routes
            val routes = jsonResponse.getJSONArray("routes")
            val legs = routes.getJSONObject(0).getJSONArray("legs")
            val steps = legs.getJSONObject(0).getJSONArray("steps")
            for (i in 0 until steps.length()) {
                val points = steps.getJSONObject(i).getJSONObject("polyline").getString("points")
                path.add(PolyUtil.decode(points))
            }
            for (i in 0 until path.size) {
                this.mMap!!.addPolyline(PolylineOptions().addAll(path[i]).color(Color.RED))
            }
        }, Response.ErrorListener {e ->
        }){

        }
        val requestQueue = Volley.newRequestQueue(requireContext())
        requestQueue.add(directionsRequest)

    }

    override fun onPause() {
        super.onPause()
        if (client != null) {
            client!!.disconnect()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    private fun getAPIClientInstance(): GoogleApiClient? {
        return GoogleApiClient.Builder(requireContext())
            .addApi(LocationServices.API).build()
    }

    private fun requestGPSSettings() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        locationRequest.interval = 2000
        locationRequest.fastestInterval = 500
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result = LocationServices.SettingsApi.checkLocationSettings(
            client!!, builder.build()
        )
        result.setResultCallback { result1: LocationSettingsResult ->
            val status = result1.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> Log.i(
                    "",
                    "All location settings are satisfied."
                )
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    Log.i(
                        "",
                        "Location settings are not satisfied. Show the user a dialog to" + "upgrade location settings "
                    )
                    try {
                        status.startResolutionForResult(requireActivity(), REQUEST_CHECK_SETTINGS)
                    } catch (e: SendIntentException) {
                        Log.e("Applicationsett", e.toString())
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    Log.i(
                        "",
                        "Location settings are inadequate, and cannot be fixed here. Dialog " + "not created."
                    )
                    Toast.makeText(
                        context,
                        "Location settings are inadequate, and cannot be fixed here",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

}