package com.rahul.mapsdemo

import android.Manifest
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class LiveLocation : Fragment() , OnMapReadyCallback {

    private var mMap: GoogleMap? = null

    private var marker: Marker? = null

    private val points: Queue<LatLng> = LinkedList()

    private var animatorSet = AnimatorSet()

    private var latitude = 0.0

    private  var longitude: Double = 0.0

    private var gpsTracker: GpsTracker? = null

    private var client: GoogleApiClient? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.maps) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap=p0
        gpsTracker = GpsTracker(requireContext())
        if (gpsTracker!!.canGetLocation()) {
            latitude = gpsTracker!!.getLatitude()
            longitude = gpsTracker!!.getLongitude()
        }

        mMap!!.addMarker(MarkerOptions().position(LatLng(latitude,longitude))
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.scooter))
            .title("My Location"))
        mMap!!.animateCamera(CameraUpdateFactory.newLatLng(LatLng(latitude,longitude)))
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude,longitude), 14.5f))

        startLocationUpdates(latitude,longitude)


    }

    private fun startLocationUpdates(lat:Double, long :Double) {

        // Create the location request to start receiving updates
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.smallestDisplacement = 100f
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        // Check whether location settings are satisfied
        val settingsClient = LocationServices.getSettingsClient(requireContext())
        settingsClient.checkLocationSettings(locationSettingsRequest)

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        getFusedLocationProviderClient(requireContext()).requestLocationUpdates(
            mLocationRequest, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    points.add(
                        LatLng(
                            lat,
                            long
                        )
                    )
                    subscribetoTimer(points)
                }
            },
            Looper.myLooper()
        )
    }

    private fun subscribetoTimer(points: Queue<LatLng>) {
        Observable.interval(5, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { SendNextPoints(points) }
    }

    private fun SendNextPoints(points1: Queue<LatLng>) {
        if (!animatorSet.isRunning && !points.isEmpty()) UpdateMarker(points1.poll())
    }

    private fun UpdateMarker(newlatlng: LatLng) {
        if (marker != null) {
            val bearingangle: Float = Calculatebearingagle(newlatlng)
            marker!!.setAnchor(0.5f, 0.5f)
            animatorSet = AnimatorSet()
            animatorSet.playTogether(
                rotateMarker(
                    (if (java.lang.Float.isNaN(bearingangle)) -1 else bearingangle) as Float,
                    marker!!.rotation
                ), moveVehicle(newlatlng, marker!!.position)
            )
            animatorSet.start()
        } else {
            AddMarker(newlatlng)
        }
            mMap!!.animateCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder().target(newlatlng)
                        .zoom(16f).build()
                )
            )
    }


    private fun AddMarker(initialpos: LatLng) {
        val markerOptions = MarkerOptions().position(initialpos)
        marker = mMap!!.addMarker(markerOptions)
    }

    private fun Calculatebearingagle(newlatlng: LatLng): Float {
        val destinationLoc = Location("service Provider")
        val userLoc = Location("service Provider")
        userLoc.latitude = marker!!.position.latitude
        userLoc.longitude = marker!!.position.longitude
        destinationLoc.latitude = newlatlng.latitude
        destinationLoc.longitude = newlatlng.longitude
        return userLoc.bearingTo(destinationLoc)
    }

    @Synchronized
    fun rotateMarker(toRotation: Float, startRotation: Float): ValueAnimator? {
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = 1555
        valueAnimator.addUpdateListener { valueAnimator ->
            val t = valueAnimator.animatedValue.toString().toFloat()
            val rot = t * toRotation + (1 - t) * startRotation
            marker!!.rotation = if (-rot > 180) rot / 2 else rot
        }
        return valueAnimator
    }

    @Synchronized
    fun moveVehicle(finalPosition: LatLng, startPosition: LatLng): ValueAnimator? {
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = 3000
        valueAnimator.addUpdateListener { valueAnimator ->
            val t = valueAnimator.animatedValue.toString().toFloat()
            val currentPosition = LatLng(
                startPosition.latitude * (1 - t) + finalPosition.latitude * t,
                startPosition.longitude * (1 - t) + finalPosition.longitude * t
            )
            marker!!.position = currentPosition
        }
        return valueAnimator
    }


}